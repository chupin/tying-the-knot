# Tying the knot

Here is a little problem that shows a nice use of laziness to be
solved.

> Given the data type:
>
> ```haskell
> data Tree = Leaf Int | Node Int Tree Tree
> ```
>
> Write a function that, in a single traversal of the tree, makes a new tree
> with the same shape as the original and where every node and leaf carries 
> the sum of the values carried by the original tree.

For example:
```
     9
    / \
   /   \
  7     8
 / \   / \
1  2  3   4
         / \
        5   6
```
gets mapped to:
```
      45
     /  \
    /    \
   /      \
  45      45
 /  \    /  \
45  45  45  45
           /  \
          45  45
```

This repository contains 3 implementation: one in Haskell (a lazy language),
one in OCaml and one in Idris (two strict languages with the
possibility to have explicitly lazy values).

The Idris one is a direct port of the OCaml version. Unfortunately, it
won't pass the totality checker. It would be nice to see if there is a
solution that'd pass it.