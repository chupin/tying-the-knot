type lazy_int = int Lazy.t

type tree =
  | Leaf of lazy_int
  | Node of lazy_int * tree * tree

let rec print_tree ff tree =
  let open Format in
  match tree with
  | Leaf(n) ->
     fprintf ff "@[(%d)@]" (Lazy.force n)
  | Node(n, t1, t2) ->
     fprintf ff "@[(%a (%d) %a)@]" print_tree t1 (Lazy.force n) print_tree t2

(* rec_sum_aux : tree -> lazy_int -> tree * lazy_int *)
let rec rec_sum_aux tree s =
  match tree with
  | Leaf(n) -> (Leaf s, n)
  | Node(n, t1, t2) ->
     let (t1, s1) = rec_sum_aux t1 s in
     let (t2, s2) = rec_sum_aux t2 s in
     (Node(s, t1, t2), lazy (Lazy.force n + Lazy.force s1 + Lazy.force s2))

(* The « obvious » way to tie the knot doesn't work because of
   syntactic restrictions on let/rec. This:

let rec_sum tree =
  let rec res = rec_sum_aux tree (snd res) in
  fst res

  will fail with « this expression is not allowed on the right-hand side of a let/rec

  To get around this limitation, we can use a fix point combinator: *)

(* fix : ('a lazy_t -> 'a) -> 'a *)
let rec fix f = f (lazy (fix f))

let map_lazy f v =
  lazy (f (Lazy.force v))

let rec_sum tree =
  let rec_fun res =
    let (res_tree, lazy_int) = rec_sum_aux tree (map_lazy snd res) in
    (res_tree, Lazy.force lazy_int) in
  fix rec_fun

let test =
  Node(lazy 0,
       Node(lazy 1,
            Node(lazy 2,
                 Leaf (lazy 3),
                 Node(lazy 4,
                      Leaf (lazy 5),
                      Leaf (lazy 12))),
             Node(lazy 6,
                  Node(lazy 7,
                       Leaf (lazy 8),
                       Leaf (lazy 9)),
                  Leaf (lazy 10))),
        Leaf (lazy 11))

let () =
  let (new_tree, _) = rec_sum test in
  let std_print tree =
    Format.printf "%a@." print_tree tree in
  std_print test;
  std_print new_tree;
