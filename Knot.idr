module Knot

-- This Idris version is a direct port of the OCaml one. It is fairly
-- nicer, since Lazy and Force can be inferred by the compiler.
--
-- Because of fix though, this won't pass the totality checker :( It'd
-- be nice to see if it's possible to make a version that does

data Tree : Type where
  Leaf : Lazy Int -> Tree
  Node : Lazy Int -> Tree -> Tree -> Tree

recSumAux : Tree -> Lazy Int -> (Tree, Lazy Int)
recSumAux (Leaf n) s = (Leaf s, n)
recSumAux (Node n t1 t2) s =
  let (t1', s1) = recSumAux t1 s
      (t2', s2) = recSumAux t2 s
  in (Node s t1' t2', n + s1 + s2)

fix : (Lazy a -> a) -> a
fix f = f (fix f)

recSum : Tree -> Tree
recSum tree =
  fst (fix (\res => recSumAux tree (snd res)))

tree : Tree
tree = Node 0 (Node 1 (Node 2 (Leaf 3) (Node 4 (Leaf 5) (Leaf 12)))
                      (Node 6 (Node 7 (Leaf 8) (Leaf 9)) (Leaf 10)))
              (Leaf 11)
