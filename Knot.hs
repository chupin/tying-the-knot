module Knot where

data Tree = Leaf Int | Node Int Tree Tree

instance Show Tree where
  show (Leaf n)       = "(" ++ show n ++ ")"
  show (Node n t1 t2) = "(" ++ show t1 ++ "(" ++ show n ++ ")" ++ show t2 ++ ")"

recSum :: Tree -> Tree
recSum tree = ntree
  where (ntree, s) = recSumAux tree s

-- s is the thunk that contains the sum of the element of the tree. It
-- doesn't need to be evaluated before the end of the traversal of the
-- tree, so its value can be constructed as we go along, and then we
-- just have to « tie the knot » (see recSum)
recSumAux :: Tree -> Int -> (Tree, Int)
recSumAux (Leaf n) s = (Leaf s, n)
recSumAux (Node n t1 t2) s = (Node s t1' t2', n + s1 + s2)
  where (t1', s1) = recSumAux t1 s
        (t2', s2) = recSumAux t2 s

test :: Tree
test = Node 0 (Node 1 (Node 2 (Leaf 3) (Node 4 (Leaf 5) (Leaf 12)))
                      (Node 6 (Node 7 (Leaf 8) (Leaf 9)) (Leaf 10)))
              (Leaf 11)
